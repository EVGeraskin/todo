import {
  findItemById,
  findParentItemByChildId,
  findInAllChilds,
  TodoItemType,
} from "./todo-reducer"

describe("todo-reducer tests", () => {
  const initailItems: Array<TodoItemType> = [
    {
      id: 0,
      title: "Hello",
      description: "hello",
      expire: "2021-07-30",
      priority: "low",
      complete: false,
      child: [
        {
          id: 1,
          title: "ns",
          description: "ns",
          expire: "2021-07-30",
          child: undefined,
          priority: "low",
          complete: false,
        },
        {
          id: 2,
          title: "ss",
          description: "ss",
          expire: "2021-07-30",
          priority: "low",
          complete: false,
          child: [
            {
              id: 3,
              title: "sss",
              description: "sss",
              expire: "2021-07-30",
              child: undefined,
              priority: "low",
              complete: false,
            },
          ],
        },
      ],
    },
    {
      id: 4,
      title: "ns",
      description: "ns",
      expire: "2021-07-30",
      child: undefined,
      priority: "low",
      complete: false,
    },
  ]
  test("findItemById should return selected item from array", () => {
    const findResult = findItemById(initailItems, 3)
    expect(findResult).not.toBeUndefined()
    expect(findResult?.id).toBe(3)
  })
  test("findParentItemByChildId should return parent item from array", () => {
    const findResult = findParentItemByChildId(initailItems, 3)
    expect(findResult).not.toBeUndefined()
    expect(findResult?.id).toBe(2)
  })

  test("hasInAllChilds should return object from child's ", () => {
    const find = findItemById(initailItems, 3)
    let findResult
    if (find) findResult = findInAllChilds(initailItems[0], find)
    expect(findResult).not.toBeUndefined()
    expect(findResult?.id).toBe(3)
  })
})
