import { createSelector } from "@reduxjs/toolkit"
import { RootState } from "./store"
import { findParentItemByChildId, TodoItemType } from "./todo-reducer"

export const selectTodoItems = (state: RootState): Array<TodoItemType> =>
  state.todo.todoItems

export const selectParentById = (id: number) =>
  createSelector(selectTodoItems, (todos): number | undefined => {
    const parent = findParentItemByChildId(todos, id)
    if (parent) return parent.id
    return undefined
  })

type Parent = {
  id: number
  title: string
}

export const selectParents = (exclude: number) =>
  createSelector(
    selectTodoItems,
    (todoItems: Array<TodoItemType>): Array<Parent> => {
      const parents: Array<Parent> = []
      const reqCycle = (arr: Array<TodoItemType>) => {
        for (let i = 0; i < arr.length; i += 1) {
          if (arr[i].id !== exclude && !arr[i].complete)
            parents.push({ id: arr[i].id, title: arr[i].title })

          const { child } = arr[i]
          if (child?.length) reqCycle(child)
        }
      }
      reqCycle(todoItems)
      return parents
    }
  )
