import { createSlice, PayloadAction } from "@reduxjs/toolkit"

export type Priority = "low" | "mid" | "hi"

export interface TodoItemType {
  id: number
  title: string
  description: string
  expire: string
  child: Array<TodoItemType> | undefined
  priority: Priority
  complete: boolean
}

export interface TodoState {
  todoItems: Array<TodoItemType>
  trashItems: Array<TodoItemType>
  lastId: number
}

const initialState: TodoState = {
  todoItems: [],
  trashItems: [],
  lastId: -1,
}

export const findItemById = (
  items: Array<TodoItemType>,
  id: number
): TodoItemType | undefined => {
  for (let i = 0; i < items.length; i += 1) {
    const { child } = items[i]

    if (child) {
      const result = findItemById(child, id)
      if (result) return result
    }

    if (items[i].id === id) return items[i]
  }
  return undefined
}

export const findParentItemByChildId = (
  items: Array<TodoItemType>,
  id: number
): TodoItemType | undefined => {
  for (let i = 0; i < items.length; i += 1) {
    const { child } = items[i]

    if (child && child.length) {
      for (let j = 0; j < child.length; j += 1) {
        if (child[j].id === id) return items[i]
      }
      const result = findParentItemByChildId(child, id)
      if (result) return result
    }
  }
  return undefined
}

export const findInAllChilds = (
  baseTodoItem: TodoItemType,
  findChild: TodoItemType
): TodoItemType | undefined => {
  if (baseTodoItem.child) {
    for (let i = 0; i < baseTodoItem.child.length; i += 1) {
      if (baseTodoItem.child[i] === findChild) return baseTodoItem.child[i]
      if (baseTodoItem.child[i].child)
        return findInAllChilds(baseTodoItem.child[i], findChild)
    }
  }
  return undefined
}

const setCompleteForAllChilds = (todoItem: TodoItemType, complete: boolean) => {
  if (todoItem.child) {
    todoItem.child.forEach((item) => {
      // eslint-disable-next-line no-param-reassign
      item.complete = complete
      if (item.child) setCompleteForAllChilds(item, complete)
    })
  }
}

const todoSlice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    clearAll() {
      return { todoItems: [], trashItems: [], lastId: -1 }
    },
    addTodo(state) {
      const newTodo: TodoItemType = {
        id: state.lastId + 1,
        title: "Заголовок",
        description: "Описание",
        child: undefined,
        priority: "low",
        expire: "edit",
        complete: false,
      }
      state.lastId += 1
      state.todoItems.push(newTodo)
    },
    changeParent(
      state,
      action: PayloadAction<{ id: number; todoParentId: number }>
    ) {
      const currentTodoItem = findItemById(state.todoItems, action.payload.id)
      const targetTodoItem = findItemById(
        state.todoItems,
        action.payload.todoParentId
      )
      const oldParentTodoItems = findParentItemByChildId(
        state.todoItems,
        action.payload.id
      )

      if (
        targetTodoItem &&
        currentTodoItem &&
        targetTodoItem !== oldParentTodoItems
      ) {
        /* Delete from parent child array link to current */
        if (oldParentTodoItems && oldParentTodoItems.child)
          oldParentTodoItems.child = oldParentTodoItems.child.filter(
            (item) => item.id !== currentTodoItem.id
          )

        /* Add currentTodoItem to target */
        if (!targetTodoItem.child) targetTodoItem.child = []
        targetTodoItem.child?.push(currentTodoItem)

        /* If we contain targetTodoItem as child in all child's levels, replace all child's from currentTodoItem to top level */
        if (findInAllChilds(currentTodoItem, targetTodoItem)) {
          currentTodoItem.child?.forEach((item) => state.todoItems.push(item))
          currentTodoItem.child = undefined
        }

        /* Remove targetTodoItem from child */
        currentTodoItem.child = currentTodoItem.child?.filter(
          (item) => item.id !== targetTodoItem.id
        )

        /* Remove currentTodoItem from top */
        state.todoItems = state.todoItems.filter(
          (item) => item !== currentTodoItem
        )
      }
    },
    deleteTodoItem(state, action: PayloadAction<number>) {
      const currentTodoItem = findItemById(state.todoItems, action.payload)
      const parentTodoItem = findParentItemByChildId(
        state.todoItems,
        action.payload
      )
      if (currentTodoItem?.child)
        currentTodoItem.child.forEach((item) => state.todoItems.push(item))

      if (parentTodoItem) {
        parentTodoItem.child = parentTodoItem.child?.filter(
          (item) => item !== currentTodoItem
        )
      } else {
        state.todoItems = state.todoItems.filter(
          (item) => item !== currentTodoItem
        )
      }
    },
    setComplete(
      state,
      action: PayloadAction<{ id: number; complete: boolean }>
    ) {
      const currentTodoItem = findItemById(state.todoItems, action.payload.id)
      const parentTodoItem = findParentItemByChildId(
        state.todoItems,
        action.payload.id
      )
      if (currentTodoItem) {
        currentTodoItem.complete = action.payload.complete

        setCompleteForAllChilds(currentTodoItem, action.payload.complete)

        if (parentTodoItem && parentTodoItem.child) {
          parentTodoItem.child = parentTodoItem.child.filter(
            (item) => item !== currentTodoItem
          )
          state.todoItems.push(currentTodoItem)
        }
      }
    },
    deleteParent(state, action: PayloadAction<number>) {
      const currentTodoItem = findItemById(state.todoItems, action.payload)
      const parentTodoItem = findParentItemByChildId(
        state.todoItems,
        action.payload
      )

      if (currentTodoItem && parentTodoItem) {
        state.todoItems.push(currentTodoItem)
        parentTodoItem.child = parentTodoItem.child?.filter(
          (item) => item.id !== action.payload
        )
      }
    },
    editTodo(state, action: PayloadAction<TodoItemType>) {
      const editTodoItem = findItemById([...state.todoItems], action.payload.id)

      if (editTodoItem) {
        editTodoItem.title = action.payload.title
        editTodoItem.priority = action.payload.priority
        editTodoItem.description = action.payload.description
        editTodoItem.expire = action.payload.expire
      }
    },
    saveToLocalStorage(state) {
      localStorage.setItem("todo/redux/todo-state", JSON.stringify(state))
    },
    loadFromLocalStorage(state) {
      const localState = localStorage.getItem("todo/redux/todo-state")

      if (localState?.length && localState.length > 10) {
        const parsed = JSON.parse(localState)
        if (
          parsed.todoItems !== undefined &&
          parsed.trashItems !== undefined &&
          parsed.lastId !== undefined
        ) {
          return { ...parsed }
        }
      }
      return state
    },
  },
})

export const {
  clearAll,
  addTodo,
  deleteTodoItem,
  setComplete,
  editTodo,
  changeParent,
  deleteParent,
  saveToLocalStorage,
  loadFromLocalStorage,
} = todoSlice.actions

export default todoSlice.reducer
