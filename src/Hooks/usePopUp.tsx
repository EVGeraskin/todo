import React, { useEffect, useState } from "react"
import PopUpText from "../Components/Tools/PopUpText"

const usePopUp = (
  targetRef: React.RefObject<HTMLDivElement>,
  text: string,
  timerInterval = 1000
) => {
  const [popUpText, setPopUpText] = useState<string>("")
  useEffect(() => {
    let timer: NodeJS.Timeout
    const handleMouseOver = () => {
      timer = setTimeout(() => {
        setPopUpText(text)
      }, timerInterval)
    }

    const handleMouseOut = () => {
      if (timer) clearTimeout(timer)
      setPopUpText("")
    }

    targetRef.current?.addEventListener("mouseover", handleMouseOver)
    targetRef.current?.addEventListener("mouseout", handleMouseOut)
    return () => {
      targetRef.current?.removeEventListener("mouseover", handleMouseOver)
      targetRef.current?.removeEventListener("mouseout", handleMouseOut)
    }
  }, [])

  return <div>{popUpText.length > 0 && <PopUpText text={popUpText} />}</div>
}

export default usePopUp
