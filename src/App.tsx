import React, { useEffect } from "react"
import "./App.css"
import { useDispatch, useSelector } from "react-redux"
import { DndProvider } from "react-dnd"
import { HTML5Backend } from "react-dnd-html5-backend"
import {
  addTodo,
  clearAll,
  loadFromLocalStorage,
  saveToLocalStorage,
  TodoItemType,
} from "./redux/todo-reducer"
import TodoItem from "./Components/TodoItem/TodoItem"
import { selectTodoItems } from "./redux/todo-selectors"
import CompletedItem from "./Components/TodoItem/CompletedItem"

const App = (): JSX.Element => {
  const todo = useSelector(selectTodoItems)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(loadFromLocalStorage())

    const saveToLocal = () => {
      dispatch(saveToLocalStorage())
    }

    window.addEventListener("beforeunload", saveToLocal)
    return () => {
      window.removeEventListener("beforeunload", saveToLocal)
    }
  }, [])

  const initTodos = (todoItems: Array<TodoItemType>, childLevel = 0) => {
    const arr: Array<JSX.Element> = []

    todoItems.forEach((item) => {
      if (item.child) {
        if (item.complete) arr.push(...initTodos(item.child, childLevel + 1))
        else arr.unshift(...initTodos(item.child, childLevel + 1))
      }
      if (item.complete) {
        arr.push(
          <CompletedItem
            key={item.id}
            todoItem={item}
            childLevel={childLevel}
          />
        )
      } else {
        arr.unshift(
          <TodoItem key={item.id} todoItem={item} childLevel={childLevel} />
        )
      }
    })
    return arr
  }

  const onClickAddButton = () => {
    dispatch(addTodo())
  }
  const onClickClearAll = () => {
    dispatch(clearAll())
  }

  return (
    <DndProvider backend={HTML5Backend}>
      <div className="App">
        <button type="button" onClick={onClickAddButton}>
          Add
        </button>
        <button type="button" onClick={onClickClearAll}>
          Clear
        </button>
        {initTodos(todo)}
      </div>
    </DndProvider>
  )
}

export default App
