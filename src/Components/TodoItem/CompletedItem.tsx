import React from "react"
import { useDispatch } from "react-redux"
import { setComplete, TodoItemType } from "../../redux/todo-reducer"
import Styled from "./style"

interface TrashItemProps {
  todoItem: TodoItemType
  childLevel: number
}

const CompletedItem: React.FC<TrashItemProps> = ({ todoItem, childLevel }) => {
  const { id, title, description, expire, priority } = todoItem
  const dispatch = useDispatch()

  const onClickRestore = () => {
    dispatch(setComplete({ id, complete: false }))
  }

  return (
    <Styled.Disabled>
      <Styled.TodoItem marginMultiplier={childLevel} priority={priority}>
        <div aria-disabled>
          <Styled.Title>{title}</Styled.Title>
          <Styled.Description>{description}</Styled.Description>
          <Styled.Expire>{expire}</Styled.Expire>
          <Styled.Priority priority={priority}>
            <div />
          </Styled.Priority>
          <Styled.Edit>
            <Styled.Enabled>
              <button type="button" onClick={onClickRestore}>
                Restore
              </button>
            </Styled.Enabled>
          </Styled.Edit>
        </div>
      </Styled.TodoItem>
    </Styled.Disabled>
  )
}

export default CompletedItem
