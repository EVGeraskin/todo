import React, { useRef, useState } from "react"
import { useDispatch } from "react-redux"
import { DropTargetMonitor, useDrag, useDrop } from "react-dnd"
import {
  changeParent,
  deleteParent,
  TodoItemType,
} from "../../redux/todo-reducer"
import Styled from "./style"
import TodoEditForm from "./TodoEditForm"
import TodoForm from "./TodoForm"

interface TodoItemProps {
  todoItem: TodoItemType
  childLevel: number
}

interface TodoItemDropResult {
  id: number
  title: string
}

const TodoItem: React.FC<TodoItemProps> = ({ todoItem, childLevel }) => {
  const { id, title, description, expire, child, priority, complete } = todoItem
  const [editMode, setEditMode] = useState<boolean>(expire === "edit")
  const [editModeTodoPriority, setEditModeTodoPriority] = useState(priority)
  const dispatch = useDispatch()

  const ref = useRef<HTMLDivElement>(null)

  const [, dragRef] = useDrag(() => ({
    type: "TodoItem",
    end: (item, motion) => {
      const dropResult: TodoItemDropResult | null = motion.getDropResult()
      if (dropResult && dropResult.id !== id) {
        dispatch(changeParent({ id, todoParentId: dropResult.id }))
      } else dispatch(deleteParent(id))
    },
  }))
  const [, dropRef] = useDrop(() => ({
    accept: "TodoItem",
    drop: () => {
      return { id, title }
    },

    collect: (monitor: DropTargetMonitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  }))

  dropRef(dragRef(ref))

  const getChildsCount = (
    childs: Array<TodoItemType>,
    exclude: TodoItemType
  ) => {
    let acc = 0
    childs.forEach((item) => {
      acc += 1
      if (item.child && item !== exclude) {
        acc += getChildsCount(item.child, exclude)
      }
    })
    return acc
  }

  const drawForm = () => {
    return (
      <div ref={ref}>
        {editMode ? (
          <TodoEditForm
            todoItem={todoItem}
            setEditModeTodoPriority={setEditModeTodoPriority}
            editModeTodoPriority={editModeTodoPriority}
            setEditMode={setEditMode}
            childLevel={childLevel}
          />
        ) : (
          <TodoForm
            title={title}
            description={description}
            expire={expire}
            childLevel={childLevel}
            priority={priority}
            setEditMode={setEditMode}
          />
        )}
      </div>
    )
  }

  const drawConnectionLines = () => (
    <div>
      <Styled.ConnectionHorizontalLine
        horizontalMultiplier={childLevel}
        verticalMultiplier={child?.length || 0}
      />

      {child && (
        <Styled.ConnectionVerticalLine
          horizontalMultiplier={childLevel}
          verticalMultiplier={getChildsCount(child, child[0]) - 1}
        />
      )}
    </div>
  )

  return (
    <>
      {complete ? (
        <Styled.Disabled ref={ref}> {drawForm()} </Styled.Disabled>
      ) : (
        <div ref={ref}>
          {drawConnectionLines()}
          {drawForm()}
        </div>
      )}
    </>
  )
}

export default TodoItem
