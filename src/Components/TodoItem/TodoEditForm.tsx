import React, { useState } from "react"
import { Field, Form, Formik } from "formik"
import { useDispatch, useSelector } from "react-redux"
import * as Yup from "yup"
import Styled from "./style"
import {
  changeParent,
  deleteParent,
  deleteTodoItem,
  editTodo,
  Priority,
  setComplete,
  TodoItemType,
} from "../../redux/todo-reducer"
import { selectParentById, selectParents } from "../../redux/todo-selectors"
import ModalWindow from "../Tools/ModalWindow"

const TodoEditFormSchema = Yup.object().shape({
  title: Yup.string().required("Required").max(40),
  description: Yup.string().required("Required").max(2200),
  editExpire: Yup.date().required("Required"),
})

interface OnChangeEventTarget extends EventTarget {
  value: Priority
}

interface FormSelectEvent extends React.FormEvent<HTMLSelectElement> {
  target: OnChangeEventTarget
}

interface FormikFormValues {
  title: string
  description: string
  editExpire: string
  todoParentId: number | undefined
}

interface TodoEditFormProps {
  todoItem: TodoItemType
  childLevel: number
  editModeTodoPriority: Priority
  setEditModeTodoPriority: (arg0: Priority) => void
  setEditMode: (arg0: boolean) => void
}

const TodoEditForm: React.FC<TodoEditFormProps> = ({
  todoItem,
  childLevel,
  editModeTodoPriority,
  setEditModeTodoPriority,
  setEditMode,
}) => {
  const { id, title, description, expire, child, priority } = todoItem
  const [deleteModalIsShown, showDeleteModal] = useState<boolean>(false)
  const [completeModalIsShown, showCompleteModal] = useState<boolean>(false)
  const dispatch = useDispatch()
  const todoParentId = useSelector(selectParentById(id))
  const parents = useSelector(selectParents(id))
  let editExpire = expire

  if (editExpire === "edit") {
    const nowDate = new Date()
    editExpire = `${nowDate.getFullYear()}-${String(
      nowDate.getMonth()
    ).padStart(2, "0")}-${String(nowDate.getDate()).padStart(2, "0")}`
  }

  const onClickSave = (values: FormikFormValues) => {
    setEditMode(false)
    dispatch(
      editTodo({
        id,
        title: values.title,
        description: values.description,
        expire: values.editExpire,
        child,
        priority: editModeTodoPriority,
        complete: false,
      })
    )
    if (values.todoParentId !== todoParentId) {
      if (Number(values.todoParentId) === -1) {
        dispatch(deleteParent(id))
      } else
        dispatch(
          changeParent({ id, todoParentId: Number(values.todoParentId) })
        )
    }
  }

  const onSelect = (event: FormSelectEvent) => {
    setEditModeTodoPriority(event.target.value)
  }

  const onClickDelete = () => {
    showDeleteModal(true)
  }

  const onClickComplete = () => {
    showCompleteModal(true)
  }

  return (
    <>
      {deleteModalIsShown && (
        <ModalWindow
          title="Внимание!"
          description="Данный элемент будет удалён. Вы уверены?"
          firstCallback={() => dispatch(deleteTodoItem(id))}
          secondCallback={() => showDeleteModal(false)}
          closeCallback={() => showDeleteModal(false)}
        />
      )}

      {completeModalIsShown && (
        <ModalWindow
          title="Внимание!"
          description="Так же будут завершены все дочерние элементы. Вы уверены?"
          firstCallback={() => dispatch(setComplete({ id, complete: true }))}
          secondCallback={() => showCompleteModal(false)}
          closeCallback={() => showCompleteModal(false)}
        />
      )}

      <Formik
        initialValues={{ title, description, editExpire, todoParentId }}
        validationSchema={TodoEditFormSchema}
        onSubmit={(values: FormikFormValues) => onClickSave(values)}
      >
        {({ errors }) => (
          <Styled.TodoItem marginMultiplier={childLevel} priority={priority}>
            <Form>
              <Styled.Title>
                <Field name="title" type="text" />
              </Styled.Title>

              <Styled.Description>
                <Field name="description" as="textarea" />
              </Styled.Description>

              <Styled.Expire>
                <Field name="editExpire" type="date" />
              </Styled.Expire>

              <Styled.Priority priority={editModeTodoPriority}>
                <Field
                  name="priority"
                  as="select"
                  onChange={onSelect}
                  value={editModeTodoPriority}
                >
                  <option value="hi">Hi priority</option>
                  <option value="mid">Mid priority</option>
                  <option value="low">Low priority</option>
                </Field>
              </Styled.Priority>
              <Styled.Parent>
                <Field name="todoParentId" as="select">
                  <option key={-1} value={-1}>
                    ---
                  </option>
                  {parents.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.title}
                    </option>
                  ))}
                </Field>
              </Styled.Parent>
              <Styled.Edit type="submit">Save</Styled.Edit>
              <Styled.ButtonDelete type="button" onClick={onClickDelete}>
                Delete
              </Styled.ButtonDelete>

              <Styled.ButtonComplete type="button" onClick={onClickComplete}>
                Complete
              </Styled.ButtonComplete>
            </Form>
          </Styled.TodoItem>
        )}
      </Formik>
    </>
  )
}

export default TodoEditForm
