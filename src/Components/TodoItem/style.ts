import styled from "styled-components"
import { Priority } from "../../redux/todo-reducer"

enum Colors {
  PRIORITY_LOW = "green",
  PRIORITY_MID = "#4159af",
  PRIORITY_HI = "red",
  TODO_BACKGROUND = "white",
}

enum Sizes {
  TODO_HEIGHT = 80,
  TODO_WIDTH = 400,
  DESCRIPTION_HEIGHT = 40,
}

type PriorityStyleProps = {
  priority: Priority
}
type DynamicMarginProps = {
  marginMultiplier: number
  priority: Priority
}

type ConnectionProps = {
  verticalMultiplier: number
  horizontalMultiplier: number
}

export const Button = styled.button`
  color: #fff;
  background-color: rgba(2, 117, 216, 0.78);
  border: 1px solid transparent;
  border-radius: 2.5px;
  :hover {
    color: black;
    border-color: black;
  }
`

const Styled = {
  Disabled: styled.div`
    pointer-events: none;
    opacity: 0.7;
  `,
  Enabled: styled.div`
    ,
    > * {
      pointer-events: auto;
    }
  `,
  Priority: styled.div<PriorityStyleProps>`
    height: 0;
    > * {
      width: 0;
      height: 0;
      border-top: 20px solid
        ${(props) =>
          // eslint-disable-next-line no-nested-ternary
          props.priority === "hi"
            ? Colors.PRIORITY_HI
            : props.priority === "mid"
            ? Colors.PRIORITY_MID
            : Colors.PRIORITY_LOW};
      border-left: 20px solid transparent;
      position: relative;
      left: 60%;
    }
  `,

  Parent: styled.div`
    grid-area: parent;
    > * {
      margin-top: 6.5px;
      height: 19px;
      width: 100%;
    }
  `,
  Expire: styled.div`
    margin-top: 8px;
    grid-area: expire;
    font-size: 10px;
    color: darkgrey;
    font-weight: bold;
    height: 14px;
    > * {
      height: 14px;
      font-size: inherit;
      color: inherit;
      font-weight: inherit;
      margin-top: -1px;
    }
  `,
  Title: styled.div`
    font-family: Open Sans, sans-serif;
    font-size: 13px;
    font-weight: bold;
    grid-area: title;
    margin-left: 4px;
    input {
      width: ${Sizes.TODO_WIDTH};
      height: 10px;
      margin-left: -4px;
      font-family: inherit;
      font-size: inherit;
      font-weight: inherit;
    }
  `,
  Description: styled.span`
      grid-area: description;
      font-size: 11px;
      height: ${Sizes.DESCRIPTION_HEIGHT}px;
    textarea {
      height: 38px;  
      width: ${Sizes.TODO_WIDTH - 51}px;
      font-size: inherit;
      resize: none;
    },
  `,

  Edit: styled(Button)`
    grid-area: edit;
    margin-top: 6.5px;
    height: 19px;
    left: 10%;
    position: relative;
  `,

  ButtonComplete: styled(Button)`
    grid-area: complete;
    margin-top: 6.5px;
    height: 19px;
    width: 100%;
  `,
  ButtonDelete: styled(Button)`
    grid-area: delete;
    color: #fff;
    background-color: rgba(210, 74, 74, 0.85);
    margin-top: 6.5px;
    height: 19px;
    width: 100%;
  `,

  TodoItem: styled.div<DynamicMarginProps>`
    > * {
      height: ${Sizes.TODO_HEIGHT}px;
      display: grid;
      grid-template: "title title title title priority" "description description description description description" "expire parent complete delete edit";
      grid-template-columns: 2.5fr 2fr 2fr 2fr 1fr;
      grid-column-gap: 1px;
      margin: 10px;
      box-shadow: 3px 3px 10px rgba(122, 122, 122, 0.8);
      margin-left: ${(props) => props.marginMultiplier * 60 + 5}px;
      width: ${Sizes.TODO_WIDTH}px;
      background-color: ${Colors.TODO_BACKGROUND};
      border-radius: 4px;
      padding: 5px 9px 5px 9px;
      border: 0.5px solid
        ${({ priority }) =>
          // eslint-disable-next-line no-nested-ternary
          priority === "mid"
            ? Colors.PRIORITY_MID
            : priority === "hi"
            ? Colors.PRIORITY_HI
            : Colors.PRIORITY_LOW};
    }
  `,
  ConnectionHorizontalLine: styled.div<ConnectionProps>`
    margin-left: ${({ horizontalMultiplier }) =>
      horizontalMultiplier * 60 - 25}px;
    width: 30px;
    height: 16px;
    border-bottom: 2px solid black;
    position: absolute;
  `,
  ConnectionVerticalLine: styled.div<ConnectionProps>`
    margin-left: ${({ horizontalMultiplier }) =>
      horizontalMultiplier * 60 + 35}px;
    margin-top: ${Sizes.TODO_HEIGHT + 11}px;
    width: 5px;
    height: ${({ verticalMultiplier }) => verticalMultiplier * 101.5 + 28}px;
    border-left: 2px solid black;
    position: absolute;
  `,
}

export default Styled
