import React, { useRef } from "react"
import Styled from "./style"
import { Priority } from "../../redux/todo-reducer"
import usePopUp from "../../Hooks/usePopUp"

export const DESCRIPTION_TEXT_LIMIT = 200

interface TodoFormProps {
  title: string
  description: string
  expire: string
  priority: Priority
  childLevel: number
  setEditMode: (arg0: boolean) => void
}

const TodoForm: React.FC<TodoFormProps> = ({
  title,
  description,
  expire,
  priority,
  childLevel,
  setEditMode,
}) => {
  const descriptionRef = useRef<HTMLDivElement>(null)
  const popUp = usePopUp(descriptionRef, description)

  const onClickEdit = () => {
    setEditMode(true)
  }

  return (
    <Styled.TodoItem marginMultiplier={childLevel} priority={priority}>
      <div>
        <Styled.Title>{title}</Styled.Title>
        <Styled.Description ref={descriptionRef}>
          {description.slice(0, DESCRIPTION_TEXT_LIMIT)}
          {description.length > DESCRIPTION_TEXT_LIMIT && " ..."}
          {description.length > DESCRIPTION_TEXT_LIMIT && popUp}
        </Styled.Description>
        <Styled.Expire>Expire: {expire}</Styled.Expire>
        <Styled.Priority priority={priority}>
          <div />
        </Styled.Priority>
        <Styled.Edit type="button" onClick={onClickEdit}>
          Edit
        </Styled.Edit>
      </div>
    </Styled.TodoItem>
  )
}

export default TodoForm
