import React from "react"
import styled from "styled-components"

const PopUpTextWindow = styled.div`
  padding: 10px;
  z-index: 999;
  background: white;
  border: black 1px solid;
  border-radius: 4px;
  position: absolute;
  width: 400px;
  height: auto;
`

const PopUpText: React.FC<{ text: string }> = ({ text }) => {
  return <PopUpTextWindow>{text}</PopUpTextWindow>
}

export default PopUpText
