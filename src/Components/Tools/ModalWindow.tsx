import React from "react"
import styled from "styled-components"
import { Button } from "../TodoItem/style"

const Styled = {
  Window: styled.div`
    z-index: 9999;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    overflow: hidden;
    position: fixed;
    top: 0px;
  `,
  Content: styled.div`
    display: grid;
    grid-template: ". title title close" ". description description description" ". yes no .";
    grid-column-gap: 5px;
    grid-template-columns: 1fr 8fr 8fr 1fr;
    grid-template-rows: 1fr 4fr 1fr;
    margin: 100px auto 0 auto;
    width: 400px;
    height: 140px;
    padding: 10px;
    background-color: #fefe;
    border-radius: 5px;
    box-shadow: 0px 0px 10px #000;
  `,
  ButtonClose: styled(Button)`
    grid-area: close;
    background-color: rgba(210, 74, 74, 0.85);
    padding: 0 5px 0 5px;
    width: 20px;
    height: 20px;
  `,
  ButtonYes: styled(Button)`
    grid-area: yes;
    background-color: rgba(210, 74, 74, 0.85);
  `,
  ButtonNo: styled(Button)`
    grid-area: no;
  `,
  Description: styled.div`
    grid-area: description;
    font-size: 14px;
  `,
  Title: styled.div`
    grid-area: title;
    font-weight: bold;
    font-size: 16px;
  `,
}

interface ModalWindowProps {
  title: string
  description: string
  firstButtonName?: string
  secondButtonName?: string
  firstCallback?: () => void
  secondCallback?: () => void
  closeCallback?: () => void
}

const ModalWindow: React.FC<ModalWindowProps> = ({
  title,
  description,
  firstButtonName = "Yes",
  secondButtonName = "No",
  firstCallback = undefined,
  secondCallback = undefined,
  closeCallback = undefined,
}) => {
  return (
    <Styled.Window>
      <Styled.Content>
        <Styled.Title>{title}</Styled.Title>
        <Styled.ButtonClose type="button" onClick={closeCallback}>
          X
        </Styled.ButtonClose>
        <Styled.Description>{description}</Styled.Description>
        <Styled.ButtonYes type="button" onClick={firstCallback}>
          Yes
        </Styled.ButtonYes>
        <Styled.ButtonNo type="button" onClick={secondCallback}>
          No
        </Styled.ButtonNo>
      </Styled.Content>
    </Styled.Window>
  )
}

export default ModalWindow
